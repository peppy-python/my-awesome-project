from datetime import datetime

def show_date_decorator(print_func):
    def wrapper(msg: str):
        print(str(datetime.now()), end=': ')
        print_func(msg)
    return wrapper

@show_date_decorator
def printmsg(msg: str):
    print(msg)


if __name__ == '__main__':
    printmsg('ololo')